import argparse
from pathlib import Path
import json
import logging

# "Word" to which to assign dictionary version.
VERSION_KEY = "__VERSION__"

# Delimeter between word and definition.
DELIMETER = "\t"

# Output filename.
OUTPUT_NAME = "DICT.js"

def read_dict(text_file: Path) -> dict:
    logging.info("Reading dictionary '%s'...", text_file)

    if not text_file.is_file():
        logging.error("Text file %s not found", text_file)
        raise FileNotFoundError(text_file)

    # WARNING: This loads all into memory.
    lines = text_file.read_text().splitlines()

    version = lines[0]

    # First line is version.
    # Second line is blank.
    lines = lines[2:]

    # Map word to definition.
    data: dict[str, str] = {}
    data[VERSION_KEY] = version
    for line in lines:
        (word, definition) = line.split(DELIMETER)
        if word in data:
            logging.warning("Dictionary already has word %s", word)
        data[word.lower()] = definition
    
    logging.info("Done reading dictionary")
    return data


def write_dict(data, js_file: Path):
    logging.info("Writing dictionary to '%s'...", js_file)
    text = "var dictionary=" + json.dumps(data)
    js_file.write_text(text)
    logging.info("Done writing dictionary")
    

def text_to_js(text_file: Path):
    js_file = text_file.parent / OUTPUT_NAME
    data = read_dict(text_file)
    write_dict(data, js_file)
    

def main():
    logging.getLogger().setLevel(logging.INFO)

    parser = argparse.ArgumentParser(
        description="Converts dictionary text file to js file.  See README.md for more details"
    )
    parser.add_argument("text_file", type=Path)
    args = parser.parse_args()
    text_to_js(args.text_file)

if __name__ == "__main__":
    main()