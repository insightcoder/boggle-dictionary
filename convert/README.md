# Convert
This script converts the text-based dictionary format into a 
`.js` format that is used in the webapp. 

Run `python txt-to-js.py DICT.txt`, where the `DICT.txt` file has
the following format:

```
Collins Scrabble Words (2019). 279,496 words with definitions.

AA	(Hawaiian) a volcanic rock consisting of angular blocks of lava with a very rough surface [n -S]
AAH	an interjection expressing surprise [interj] / to exclaim in surprise [v -ED, -ING, -S]
AAHED	AAH, to exclaim in surprise [v]
AAHING	AAH, to exclaim in surprise [v]
AAHS	AAH, to exclaim in surprise [v]
AAL	(Hindi) the Indian mulberry tree, aka noni, also AL [n -S]
AALII	(Hawaiian) a tropical tree [n -S]
AALIIS	AALII, (Hawaiian) a tropical tree [n]
...
```