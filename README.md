# Boggle Dictionary
Simple JavaScript dictionary to play Boggle.  

Insight Coder links:
- [Boggle Dictionary](http://www.insightcoder.com/boggle-dictionary/)
- [Making Boggle Dictionary](https://www.insightcoder.com/385/making-a-simple-boggle-dictionary/)

## Underlying Dictionary
Currently, only one dictionary can be used to drive the
Boggle dictionary.  In the future, I may add the ability to *select* which 
dictionary should be used, or list the number of dictionaries that are supported. 

The underlying dictionary has been updated based on the following:

### Collins Word List (CWL) 2019: May 2022 - Present
After having received several complaints that it doesn't include several larger words, 
I am updating to the Collins Word List (CWL), using sources from: https://boardgames.stackexchange.com/questions/38366/latest-collins-scrabble-words-list-in-text-file


### OWL2: 2017 - May 2022
When starting off, I found the OWL2 dictionary and started using that out of the box. 